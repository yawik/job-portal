---
title: À propos de nous
desc: Woher kommt Yawik. Was kann man damit machen. Wozu ist es gut.
components:
- pages/infos/TestComponent
---

# À propos de nous
Qu'est-ce que le Jobboard Yawik ? Et qu'est-ce que Yawik, le logiciel utilisé ? En outre, nous présentons sur cette page les personnes derrière le 
Yawik Jobboard. 

## À propos du Jobboard Yawik
Le Yawik Jobboard est un site d'emploi, mais un site particulier. Comme sur d'autres portails, il est possible de créer des annonces d'emploi et de rechercher des postes vacants. Ceux qui sont à la recherche d'un nouveau travail peuvent limiter leur sélection à certaines régions ou à certains secteurs. Il est également possible de rechercher de manière ciblée des postes vacants dans de grandes entreprises. 
Il existe néanmoins quelques différences importantes par rapport aux concurrents commerciaux. L'annonce d'emploi sur le Yawik Jobboard est gratuite. Elle peut être saisie facilement et confortablement via un formulaire et - si on le souhaite - être ensuite intégrée presque aussi facilement sur son propre site web. Autre particularité : le jobboard Yawik est basé sur le logiciel libre Yawik.

## À propos de Yawik
Yawik est aux logiciels de ressources humaines ce que WordPress est aux systèmes de rédaction : une solution open source gratuite et flexible. Il ne sert pas seulement de base au portail de l'emploi Yawik, mais peut également être utilisé pour une bourse de l'emploi personnelle ou pour les pages carrières d'une entreprise. Le caractère open source du logiciel permet de l'adapter facilement à ses propres besoins. 
Il est important que Yawik puisse être facilement adapté au design de l'entreprise. Le logiciel propose une gestion des offres d'emploi et des candidats (ATS), une base de données de CV et un portail d'emploi. Tous les éléments sont toutefois modulaires, il n'est pas nécessaire d'utiliser toutes les briques pour chaque application. De plus, le caractère open source de Yawaw permet de développer et de compléter d'autres éléments. 
Yawik permet également de publier des offres d'emploi via Google Jobs.

Traduit avec www.DeepL.com/Translator (version gratuite)

## Yawik Funktionen

- Aktuell kann man mit Yawik kostenlos Stellenazeigen auf Google Jobs veröffentlichen.
- Das ganze Jobportal im eigenen Design betreiben.
- Eine Karriereseite für ihr Unternehmen bauen.
- Bewerbungsformulare für ihre Anzeigen nutzen.
- Stellenanzeigen in eigene Homepage einbinden.

## Stellenbörse (jobboard)

Die Stellenbörse ist für den Jobsuchenden. 

## Anzeigenmanagement (jobwizard)

Das Anzeigenmanagement richtet sich an Arbeitgeber. Es ermöglicht damit die Eingabe, die Verwaltung und die Veröffentlichung von Stellenanzeigen.

## Bewerbungsformular (Application Form)

es gibt ein Bewerbungsformular unter https://form.yawik.org

Das Formular nutzt z.Z. für den Mailversand unser altes Yawik.


## Software Stack

Yawik verwendet [Quasar](https://quasar.dev) für Frontend und [Strapi](https://strapi.io) für das Backend.